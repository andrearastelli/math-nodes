import sys
import math

import maya.api.OpenMaya as OpenMaya


kNodeName = 'mathScalarArithmeticsNode'


def maya_useNewAPI():
    pass


class Operation:
    SUM = 0
    SUBTRACTION = 1
    MULTIPLICATION = 2
    DIVISION = 3
    MODULUS = 4


class ScalarArithmeticsNode(OpenMaya.MPxNode):

    id = OpenMaya.MTypeId(0x0010551F)

    inOperation = None

    inValueA = None
    inValueB = None

    outValueResult = None

    def __init__(self):
        super(ScalarArithmeticsNode, self).__init__()

    @staticmethod
    def creator():
        return ScalarArithmeticsNode()

    @staticmethod
    def initialize():
        inOperationFn = OpenMaya.MFnEnumAttribute()
        ScalarArithmeticsNode.inOperation = inOperationFn.create('Operation', 'o', Operation.SUM)
        inOperationFn.addField('plus', Operation.SUM)
        inOperationFn.addField('minus', Operation.SUBTRACTION)
        inOperationFn.addField('multiply', Operation.MULTIPLICATION)
        inOperationFn.addField('divide', Operation.DIVISION)
        inOperationFn.addField('modulus', Operation.MODULUS)
        inOperationFn.storable = True
        inOperationFn.keyable = False
        inOperationFn.readable = True
        inOperationFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarArithmeticsNode.inOperation)

        inValueAFn = OpenMaya.MFnNumericAttribute()
        ScalarArithmeticsNode.inValueA = inValueAFn.create('inValueA', 'inA', OpenMaya.MFnNumericData.kDouble, 0)
        inValueAFn.storable = True
        inValueAFn.keyable = True
        inValueAFn.readable = True
        inValueAFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarArithmeticsNode.inValueA)

        inValueBFn = OpenMaya.MFnNumericAttribute()
        ScalarArithmeticsNode.inValueB = inValueBFn.create('inValueB', 'inB', OpenMaya.MFnNumericData.kDouble, 0)
        inValueBFn.storable = True
        inValueBFn.keyable = True
        inValueBFn.readable = True
        inValueBFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarArithmeticsNode.inValueB)

        outValueResultFn = OpenMaya.MFnNumericAttribute()
        ScalarArithmeticsNode.outValueResult = outValueResultFn.create('outResult', 'res', OpenMaya.MFnNumericData.kDouble, 0)
        outValueResultFn.storable = False
        outValueResultFn.keyable = False
        outValueResultFn.readable = True
        outValueResultFn.writable = False
        OpenMaya.MPxNode.addAttribute(ScalarArithmeticsNode.outValueResult)

        OpenMaya.MPxNode.attributeAffects(ScalarArithmeticsNode.inOperation, ScalarArithmeticsNode.outValueResult)
        OpenMaya.MPxNode.attributeAffects(ScalarArithmeticsNode.inValueA, ScalarArithmeticsNode.outValueResult)
        OpenMaya.MPxNode.attributeAffects(ScalarArithmeticsNode.inValueB, ScalarArithmeticsNode.outValueResult)

    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        inValueADataHandler = data.inputValue(ScalarArithmeticsNode.inValueA)
        valueA = inValueADataHandler.asDouble()

        inValueBDataHandler = data.inputValue(ScalarArithmeticsNode.inValueB)
        valueB = inValueBDataHandler.asDouble()

        inOperationDataHandler = data.inputValue(ScalarArithmeticsNode.inOperation)
        operation = inOperationDataHandler.asShort()

        result = 0

        if operation == Operation.SUM:
            result = valueA + valueB
        elif operation == Operation.SUBTRACTION:
            result = valueA - valueB
        elif operation == Operation.MULTIPLICATION:
            result = valueA * valueB
        elif operation == Operation.DIVISION:
            result = valueA / valueB
        else:
            result = math.fmod(valueA, valueB)

        outValueResultDataHandler = data.outputValue(ScalarArithmeticsNode.outValueResult)
        outValueResultDataHandler.setDouble(result)


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")

    try:
        mplugin.registerNode(kNodeName, ScalarArithmeticsNode.id, ScalarArithmeticsNode.creator, ScalarArithmeticsNode.initialize)
    except:
        sys.stderr.write("Failed to register node %s\n" % kNodeName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(ScalarArithmeticsNode.id)
    except:
        sys.stderr.write("Failed to deregister node %s\n" % kNodeName)
        raise
