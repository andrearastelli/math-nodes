import sys
import math

import maya.api.OpenMaya as OpenMaya


kNodeName = 'mathTrigonometricNode'


def maya_useNewAPI():
    pass


class TrigonometricFunctionsNode(OpenMaya.MPxNode):

    id = OpenMaya.MTypeId(0x001054FF)

    inFunctionSel = None
    inAngleValue = None
    inAngleIsRadians = None

    outValue = None

    def __init__(self):
        super(TrigonometricFunctionsNode, self).__init__()

    @staticmethod
    def creator():
        return TrigonometricFunctionsNode()

    @staticmethod
    def initialize():
        inFunctionSelFn = OpenMaya.MFnEnumAttribute()
        TrigonometricFunctionsNode.inFunctionSel = inFunctionSelFn.create('Function', 'func', 0)
        inFunctionSelFn.addField('cos', 0)
        inFunctionSelFn.addField('sin', 1)
        inFunctionSelFn.addField('tan', 2)
        inFunctionSelFn.storable = True
        inFunctionSelFn.keyable = False
        inFunctionSelFn.readable = True
        inFunctionSelFn.writable = True
        OpenMaya.MPxNode.addAttribute(TrigonometricFunctionsNode.inFunctionSel)

        inAngleValueFn = OpenMaya.MFnNumericAttribute()
        TrigonometricFunctionsNode.inAngleValue = inAngleValueFn.create('Angle', 'angle', OpenMaya.MFnNumericData.kDouble, 0)
        inAngleValueFn.storable = True
        inAngleValueFn.keyable = False
        inAngleValueFn.readable = True
        inAngleValueFn.writable = True
        OpenMaya.MPxNode.addAttribute(TrigonometricFunctionsNode.inAngleValue)

        inAngleIsRadiansFn = OpenMaya.MFnNumericAttribute()
        TrigonometricFunctionsNode.inAngleIsRadians = inAngleIsRadiansFn.create('isRadians', 'isRad', OpenMaya.MFnNumericData.kBoolean, 0)
        inAngleIsRadiansFn.storable = True
        inAngleIsRadiansFn.keyable = False
        inAngleIsRadiansFn.readable = True
        inAngleIsRadiansFn.writable = True
        OpenMaya.MPxNode.addAttribute(TrigonometricFunctionsNode.inAngleIsRadians)

        outValueFn = OpenMaya.MFnNumericAttribute()
        TrigonometricFunctionsNode.outValue = outValueFn.create('outValue', 'out', OpenMaya.MFnNumericData.kDouble, 0)
        outValueFn.storable = False
        outValueFn.keyable = False
        outValueFn.readable = True
        outValueFn.writable = False
        OpenMaya.MPxNode.addAttribute(TrigonometricFunctionsNode.outValue)

        OpenMaya.MPxNode.attributeAffects(TrigonometricFunctionsNode.inFunctionSel, TrigonometricFunctionsNode.outValue)
        OpenMaya.MPxNode.attributeAffects(TrigonometricFunctionsNode.inAngleValue, TrigonometricFunctionsNode.outValue)
        OpenMaya.MPxNode.attributeAffects(TrigonometricFunctionsNode.inAngleIsRadians, TrigonometricFunctionsNode.outValue)

    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        inAngleValueDataHandle = data.inputValue(TrigonometricFunctionsNode.inAngleValue)
        angleValue = inAngleValueDataHandle.asDouble()

        inAngleIsRadiansDataHandle = data.inputValue(TrigonometricFunctionsNode.inAngleIsRadians)
        angleIsRadians = inAngleIsRadiansDataHandle.asBool()

        inFunctionSelDataHandle = data.inputValue(TrigonometricFunctionsNode.inFunctionSel)
        functionType = inFunctionSelDataHandle.asShort()

        if angleIsRadians:
            angleValue = math.radians(angleValue)

        result = 0

        if functionType == 0:
            result = math.cos(angleValue)
        elif functionType == 1:
            result = math.sin(angleValue)
        elif functionType == 2:
            result = math.tan(angleValue)

        outValueFn = data.outputValue(TrigonometricFunctionsNode.outValue)
        outValueFn.setDouble(result)


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.1", "Any")

    try:
        mplugin.registerNode(kNodeName, TrigonometricFunctionsNode.id, TrigonometricFunctionsNode.creator, TrigonometricFunctionsNode.initialize)
    except:
        sys.stderr.write("Failed to register node: %s\n" % kNodeName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(TrigonometricFunctionsNode.id)
    except:
        sys.stderr.write("Failed to deregister node: %s\n" % kNodeName)
        raise
