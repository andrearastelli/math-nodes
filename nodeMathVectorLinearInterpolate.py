import sys
import math

import maya.api.OpenMaya as OpenMaya


kNodeName = 'mathVectorLinearInterpolate'


def maya_useNewAPI():
    pass


class ScalarLinearInterpolate(OpenMaya.MPxNode):

    id = OpenMaya.MTypeId(0x0010553F)

    inValueA = None
    inValueB = None

    inValueInterpolation = None

    outValueResult = None

    def __init__(self):
        super(ScalarLinearInterpolate, self).__init__()

    @staticmethod
    def creator():
        return ScalarLinearInterpolate()

    @staticmethod
    def initialize():
        inValueAFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.inValueA = inValueAFn.create('inValueA', 'inA', OpenMaya.MFnNumericData.k3Double, 0)
        inValueAFn.storable = True
        inValueAFn.keyable = True
        inValueAFn.readable = True
        inValueAFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.inValueA)

        inValueBFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.inValueB = inValueBFn.create('inValueB', 'inB', OpenMaya.MFnNumericData.k3Double, 0)
        inValueBFn.storable = True
        inValueBFn.keyable = True
        inValueBFn.readable = True
        inValueBFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.inValueB)

        inValueInterpolationFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.inValueInterpolation = inValueInterpolationFn.create('inValueInterpolation', 'inInterp', OpenMaya.MFnNumericData.kDouble, 0.5)
        inValueInterpolationFn.storable = True
        inValueInterpolationFn.keyable = True
        inValueInterpolationFn.readable = True
        inValueInterpolationFn.writable = True
        inValueInterpolationFn.setMin(0)
        inValueInterpolationFn.setMax(1)
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.inValueInterpolation)

        outValueResultFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.outValueResult = outValueResultFn.create('outResult', 'res', OpenMaya.MFnNumericData.k3Double, 0)
        outValueResultFn.storable = False
        outValueResultFn.keyable = False
        outValueResultFn.readable = True
        outValueResultFn.writable = False
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.outValueResult)

        OpenMaya.MPxNode.attributeAffects(ScalarLinearInterpolate.inValueA, ScalarLinearInterpolate.outValueResult)
        OpenMaya.MPxNode.attributeAffects(ScalarLinearInterpolate.inValueB, ScalarLinearInterpolate.outValueResult)
        OpenMaya.MPxNode.attributeAffects(ScalarLinearInterpolate.inValueInterpolation, ScalarLinearInterpolate.outValueResult)

    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        inValueADataHandle = data.inputValue(ScalarLinearInterpolate.inValueA)
        valueA = inValueADataHandle.asDouble3()

        inValueBDataHandle = data.inputValue(ScalarLinearInterpolate.inValueB)
        valueB = inValueBDataHandle.asDouble3()

        inValueInterpolationDataHandle = data.inputValue(ScalarLinearInterpolate.inValueInterpolation)
        interpolation = inValueInterpolationDataHandle.asDouble()

        result = []
        result.append(interpolation * valueA[0] + (1 - interpolation) * valueB[0])
        result.append(interpolation * valueA[1] + (1 - interpolation) * valueB[1])
        result.append(interpolation * valueA[2] + (1 - interpolation) * valueB[2])

        outValueResultDataHandle = data.outputValue(ScalarLinearInterpolate.outValueResult)
        outValueResultDataHandle.set3Double(result[0], result[1], result[2])


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")

    try:
        mplugin.registerNode(kNodeName, ScalarLinearInterpolate.id, ScalarLinearInterpolate.creator, ScalarLinearInterpolate.initialize)
    except:
        sys.stderr.write("Failed to register node %s\n" % kNodeName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(ScalarLinearInterpolate.id)
    except:
        sys.stderr.write("Failed to deregister node %s\n" % kNodeName)
        raise
