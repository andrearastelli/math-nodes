import sys
import math

import maya.api.OpenMaya as OpenMaya


kNodeName = 'mathVectorArithmeticsNode'


def maya_useNewAPI():
    pass


class Operation:
    SUM = 0
    SUBTRACTION = 1
    DOT_PRODUCT = 2
    SCALAR = 3
    VECTORIAL_PRODUCT = 4
    DISTANCE = 5
    ANGLE_BETWEEN = 6


class VectorArithmeticsNode(OpenMaya.MPxNode):

    id = OpenMaya.MTypeId(0x0010550F)

    inOperation = None

    inValueA = None
    inValueB = None

    inMakeAScalar = None

    outVectorResult = None
    outScalarResult = None

    def __init__(self):
        super(VectorArithmeticsNode, self).__init__()

    @staticmethod
    def creator():
        return VectorArithmeticsNode()

    @staticmethod
    def initialize():
        inOperationFn = OpenMaya.MFnEnumAttribute()
        VectorArithmeticsNode.inOperation = inOperationFn.create('inOperation', 'o', Operation.SUM)
        inOperationFn.addField('plus', Operation.SUM)
        inOperationFn.addField('minus', Operation.SUBTRACTION)
        inOperationFn.addField('dot product', Operation.DOT_PRODUCT)
        inOperationFn.addField('vectorial product', Operation.VECTORIAL_PRODUCT)
        inOperationFn.addField('distance', Operation.DISTANCE)
        inOperationFn.addField('angle between', Operation.ANGLE_BETWEEN)
        inOperationFn.addField('scalar multiplication', Operation.SCALAR)
        inOperationFn.storable = True
        inOperationFn.keyable = False
        inOperationFn.readable = True
        inOperationFn.writable = True
        OpenMaya.MPxNode.addAttribute(VectorArithmeticsNode.inOperation)

        inValueAFn = OpenMaya.MFnNumericAttribute()
        VectorArithmeticsNode.inValueA = inValueAFn.create('inValueA', 'inA', OpenMaya.MFnNumericData.k3Double)
        inValueAFn.storable = True
        inValueAFn.keyable = True
        inValueAFn.readable = True
        inValueAFn.writable = True
        OpenMaya.MPxNode.addAttribute(VectorArithmeticsNode.inValueA)

        inValueBFn = OpenMaya.MFnNumericAttribute()
        VectorArithmeticsNode.inValueB = inValueBFn.create('inValueB', 'inB', OpenMaya.MFnNumericData.k3Double)
        inValueBFn.storable = True
        inValueBFn.keyable = True
        inValueBFn.readable = True
        inValueBFn.writable = True
        OpenMaya.MPxNode.addAttribute(VectorArithmeticsNode.inValueB)

        inMakeAScalarFn = OpenMaya.MFnNumericAttribute()
        VectorArithmeticsNode.inMakeAScalar = inMakeAScalarFn.create('makeScalarA', 'scalarA', OpenMaya.MFnNumericData.kBoolean, 0)
        inMakeAScalarFn.storable = True
        inMakeAScalarFn.keyable = True
        inMakeAScalarFn.readable = True
        inMakeAScalarFn.writable = True
        OpenMaya.MPxNode.addAttribute(VectorArithmeticsNode.inMakeAScalar)

        outVectorResultFn = OpenMaya.MFnNumericAttribute()
        VectorArithmeticsNode.outVectorResult = outVectorResultFn.create('vectorResult', 'vres', OpenMaya.MFnNumericData.k3Double)
        outVectorResultFn.storable = False
        outVectorResultFn.keyable = False
        outVectorResultFn.readable = True
        outVectorResultFn.writable = False
        OpenMaya.MPxNode.addAttribute(VectorArithmeticsNode.outVectorResult)

        outScalarResultFn = OpenMaya.MFnNumericAttribute()
        VectorArithmeticsNode.outScalarResult = outScalarResultFn.create('scalarResult', 'sres', OpenMaya.MFnNumericData.kDouble, 0)
        outScalarResultFn.storable = False
        outScalarResultFn.keyable = False
        outScalarResultFn.readable = True
        outScalarResultFn.writable = False
        OpenMaya.MPxNode.addAttribute(VectorArithmeticsNode.outScalarResult)

        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inValueA, VectorArithmeticsNode.outVectorResult)
        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inValueB, VectorArithmeticsNode.outVectorResult)
        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inMakeAScalar, VectorArithmeticsNode.outVectorResult)
        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inOperation, VectorArithmeticsNode.outVectorResult)

        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inValueA, VectorArithmeticsNode.outScalarResult)
        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inValueB, VectorArithmeticsNode.outScalarResult)
        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inMakeAScalar, VectorArithmeticsNode.outScalarResult)
        OpenMaya.MPxNode.attributeAffects(VectorArithmeticsNode.inOperation, VectorArithmeticsNode.outScalarResult)

    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        inValueADataHandle = data.inputValue(VectorArithmeticsNode.inValueA)
        valueA = OpenMaya.MVector(inValueADataHandle.asDouble3())

        inValueBDataHandle = data.inputValue(VectorArithmeticsNode.inValueB)
        valueB = OpenMaya.MVector(inValueBDataHandle.asDouble3())

        inMakeAScalarDataHandle = data.inputValue(VectorArithmeticsNode.inMakeAScalar)
        isScalarA = inMakeAScalarDataHandle.asBool()

        inOperationDataHandle = data.inputValue(VectorArithmeticsNode.inOperation)
        operation = inOperationDataHandle.asShort()

        resultVector = OpenMaya.MVector()
        resultScalar = 0

        if operation == Operation.SUM:
            resultVector = valueA + valueB
        elif operation == Operation.SUBTRACTION:
            resultVector = valueA - valueB
        elif operation == Operation.DOT_PRODUCT:
            resultScalar = valueA * valueB
        elif operation == Operation.ANGLE_BETWEEN:
            resultScalar = valueA.angle(valueB)
        elif operation == Operation.DISTANCE:
            resultVector = valueA - valueB
            resultScalar = resultVector.length()
        elif operation == Operation.SCALAR:
            if isScalarA:
                scalarA = valueA.x
                resultScalar = scalarA * valueB

        resultVectorDataHandle = data.outputValue(VectorArithmeticsNode.outVectorResult)
        resultVectorDataHandle.set3Double(resultVector.x, resultVector.y, resultVector.z)

        resultScalarDataHandle = data.outputValue(VectorArithmeticsNode.outScalarResult)
        resultScalarDataHandle.setDouble(resultScalar)


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")

    try:
        mplugin.registerNode(kNodeName, VectorArithmeticsNode.id, VectorArithmeticsNode.creator, VectorArithmeticsNode.initialize)
    except:
        sys.stderr.write("Failed to register node %s\n" % kNodeName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(VectorArithmeticsNode.id)
    except:
        sys.stderr.write("Failed to deregister node %s\n" % kNodeName)
        raise
