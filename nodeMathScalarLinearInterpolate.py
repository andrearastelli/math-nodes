import sys
import math

import maya.api.OpenMaya as OpenMaya


kNodeName = 'mathScalarLinearInterpolate'


def maya_useNewAPI():
    pass


class ScalarLinearInterpolate(OpenMaya.MPxNode):

    id = OpenMaya.MTypeId(0x0010552F)

    inValueA = None
    inValueB = None

    inValueInterpolation = None

    outValueResult = None

    def __init__(self):
        super(ScalarLinearInterpolate, self).__init__()

    @staticmethod
    def creator():
        return ScalarLinearInterpolate()

    @staticmethod
    def initialize():
        inValueAFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.inValueA = inValueAFn.create('inValueA', 'inA', OpenMaya.MFnNumericData.kDouble, 0)
        inValueAFn.storable = True
        inValueAFn.keyable = True
        inValueAFn.readable = True
        inValueAFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.inValueA)

        inValueBFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.inValueB = inValueBFn.create('inValueB', 'inB', OpenMaya.MFnNumericData.kDouble, 0)
        inValueBFn.storable = True
        inValueBFn.keyable = True
        inValueBFn.readable = True
        inValueBFn.writable = True
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.inValueB)

        inValueInterpolationFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.inValueInterpolation = inValueInterpolationFn.create('inValueInterpolation', 'inInterp', OpenMaya.MFnNumericData.kDouble, 0.5)
        inValueInterpolationFn.storable = True
        inValueInterpolationFn.keyable = True
        inValueInterpolationFn.readable = True
        inValueInterpolationFn.writable = True
        inValueInterpolationFn.setMin(0)
        inValueInterpolationFn.setMax(1)
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.inValueInterpolation)

        outValueResultFn = OpenMaya.MFnNumericAttribute()
        ScalarLinearInterpolate.outValueResult = outValueResultFn.create('outResult', 'res', OpenMaya.MFnNumericData.kDouble, 0)
        outValueResultFn.storable = False
        outValueResultFn.keyable = False
        outValueResultFn.readable = True
        outValueResultFn.writable = False
        OpenMaya.MPxNode.addAttribute(ScalarLinearInterpolate.outValueResult)

        OpenMaya.MPxNode.attributeAffects(ScalarLinearInterpolate.inValueA, ScalarLinearInterpolate.outValueResult)
        OpenMaya.MPxNode.attributeAffects(ScalarLinearInterpolate.inValueB, ScalarLinearInterpolate.outValueResult)
        OpenMaya.MPxNode.attributeAffects(ScalarLinearInterpolate.inValueInterpolation, ScalarLinearInterpolate.outValueResult)

    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        inValueADataHandle = data.inputValue(ScalarLinearInterpolate.inValueA)
        valueA = inValueADataHandle.asDouble()

        inValueBDataHandle = data.inputValue(ScalarLinearInterpolate.inValueB)
        valueB = inValueBDataHandle.asDouble()

        inValueInterpolationDataHandle = data.inputValue(ScalarLinearInterpolate.inValueInterpolation)
        interpolation = inValueInterpolationDataHandle.asDouble()

        result = interpolation * valueA + (1 - interpolation) * valueB

        outValueResultDataHandle = data.outputValue(ScalarLinearInterpolate.outValueResult)
        outValueResultDataHandle.setDouble(result)


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")

    try:
        mplugin.registerNode(kNodeName, ScalarLinearInterpolate.id, ScalarLinearInterpolate.creator, ScalarLinearInterpolate.initialize)
    except:
        sys.stderr.write("Failed to register node %s\n" % kNodeName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(ScalarLinearInterpolate.id)
    except:
        sys.stderr.write("Failed to deregister node %s\n" % kNodeName)
        raise
